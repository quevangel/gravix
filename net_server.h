#pragma once

#include <SDL2/SDL_net.h>

#include "net_common.h"
#include "players.h"

extern int no_clients;
extern TCPsocket socket_to_client[MAX_NO_PLAYERS];
extern TCPsocket server_socket;

void handle_message (int client_index, struct message*);
void gvx_server_send_all (int i);

#define DEF_HANDLER(thing) void handle_##thing (int, struct message*);

DEF_HANDLER(query_all);
DEF_HANDLER(update_tile);
DEF_HANDLER(move_intention);
DEF_HANDLER(change_mode);

