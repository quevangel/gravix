#include "net_common.h"

#include "tiles.h"
#include "players.h"

void
gvx_send_query_all (TCPsocket socket)
{
  struct message message;
  message.type = MESSAGE_TYPE_QUERY_ALL;
  gvx_send (socket, message);
}

void
gvx_send_update_tile (TCPsocket socket, int x, int y)
{
  struct message message;
  message.type = MESSAGE_TYPE_UPDATE_TILE;
  message.tile_data.x = x;
  message.tile_data.y = y;
  message.tile_data.value = tiles[x][y];
  gvx_send (socket, message);
}

void
gvx_send_update_player (TCPsocket socket, int i)
{
  struct message message;
  message.type = MESSAGE_TYPE_UPDATE_PLAYER;
  message.player_data = player_data[i];
  gvx_send (socket, message);
}

void
gvx_send (TCPsocket socket, struct message message)
{
  SDLNet_TCP_Send (socket, &message, sizeof (message));
}

void
gvx_send_unauthorized (TCPsocket socket)
{
  struct message message;
  message.type = MESSAGE_TYPE_UNAUTHORIZED;
  gvx_send (socket, message);
}

void
gvx_send_update_index (TCPsocket socket, int index)
{
  struct message message;
  message.type = MESSAGE_TYPE_UPDATE_INDEX;
  message.index = index;
  gvx_send (socket, message);
}

void
gvx_send_update_tiles_metadata (TCPsocket socket)
{
  struct message message;
  message.type = MESSAGE_TYPE_UPDATE_TILES_METADATA;
  message.tiles_metadata = (struct message_tiles_metadata) {.width = get_tiles_width (), .height = get_tiles_height ()};
  gvx_send (socket, message);
}

void
gvx_send_move_intention (TCPsocket socket, enum move_intention move_intention)
{
  struct message message;
  message.type = MESSAGE_TYPE_MOVE_INTENTION;
  message.move_intention = move_intention;
  gvx_send (socket, message);
}

void
gvx_send_change_mode (TCPsocket socket, enum player_mode mode)
{
  struct message message;
  message.type = MESSAGE_TYPE_CHANGE_MODE;
  message.player_mode = mode;
  gvx_send (socket, message);
}
