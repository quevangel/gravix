#include "math_util.h"

#include <math.h>
#include <assert.h>

struct affine_transform
linear_interpolation (float a, float b, float a_res, float b_res)
{
  // (x - a) * (b_res - a_res) / (b - a) + a_res
  // x * (b_res - a_res) / (b - a) + (a_res * b - a * b_res) / (b - a)
  float inv_original_delta = 1.f / (b - a);
  return (struct affine_transform) {(b_res - a_res) * inv_original_delta,
    (a_res * b - a * b_res) * inv_original_delta};
}

float
apply_affine_transform (float v, struct affine_transform transform)
{
  return v * transform.multiplier + transform.adder;
}


int
next_integer (float f, int direction)
{
  switch(direction)
    {
    case 1:
      return (int) ceil (f);
    case -1:
      return (int) floor (f);
    }
  assert (0);
  return -1;
}

int
get_cell (float x, int side)
{
  switch (side)
    {
    case 1:
      return floor (x);
    case -1:
      return (int) ceil (x) - 1;
    default:
      assert (0);
    }
}
