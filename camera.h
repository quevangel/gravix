#pragma once

#include <SDL2/SDL.h>

#include "box.h"

#define WINDOW_WIDTH 600
#define WINDOW_HEIGHT 600

extern struct box camera;

void
camera_transform_point (float point[2]);

void
camera_inverse_transform_point (float point[2]);

void
camera_box_to_sdl_rect (struct box* box, SDL_Rect* rect);
