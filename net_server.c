#include "net_server.h"

#include <assert.h>

#include "net_common.h"
#include "tiles.h"


int no_clients = 0;
TCPsocket socket_to_client[MAX_NO_PLAYERS];
TCPsocket server_socket;

void
gvx_server_send_all (int player_index)
{
  TCPsocket socket = socket_to_client[player_index];
  gvx_send_update_tiles_metadata (socket);
  gvx_send_update_index (socket, player_index);
  for (int x = 0; x < get_tiles_width (); x++)
    {
      for (int y = 0; y < get_tiles_height (); y++)
	{
	  gvx_send_update_tile (socket, x, y);
	}
      SDL_Delay (5);
    }
  for (int i = 0; i < MAX_NO_PLAYERS; i++)
    {
      if (player_data[i].alive)
	{
	  gvx_send_update_player (socket, i);
	}
    }
}

void
handle_message (int client_index, struct message* message)
{
  switch (message->type)
    {
    case MESSAGE_TYPE_QUERY_ALL:
      handle_query_all (client_index, message);
      break;
    case MESSAGE_TYPE_UPDATE_TILE:
      handle_update_tile (client_index, message);
      break;
    case MESSAGE_TYPE_MOVE_INTENTION:
      handle_move_intention (client_index, message);
      break;
    case MESSAGE_TYPE_CHANGE_MODE:
      handle_change_mode (client_index, message);
      break;
    case MESSAGE_TYPE_UPDATE_PLAYER:
    case MESSAGE_TYPE_UPDATE_TILES_METADATA: 
    case MESSAGE_TYPE_UPDATE_INDEX:
    case MESSAGE_TYPE_UNAUTHORIZED:
      gvx_send_unauthorized (socket_to_client[client_index]);
      break;
    }
}

void
handle_query_all (int player_index, struct message* message)
{
  gvx_server_send_all (player_index);
}

void
handle_update_tile (int player_index, struct message* message)
{
  assert (message->type == MESSAGE_TYPE_UPDATE_TILE);
  int x = message->tile_data.x;
  int y = message->tile_data.y;
  int value = message->tile_data.value;
  tiles[x][y] = value;
  for (int i = 0; i < no_clients; i++)
    {
      gvx_send_update_tile (socket_to_client[i], x, y);
    }
}

void
handle_move_intention (int player_index, struct message* message)
{
  assert (message->type == MESSAGE_TYPE_MOVE_INTENTION);
  player_data[player_index].move_intention_bits |= (1 << message->move_intention);
}

void
handle_change_mode (int player_index, struct message* message)
{
  assert (message->type == MESSAGE_TYPE_CHANGE_MODE);
  player_data[player_index].mode = message->player_mode;
}
