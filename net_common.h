#pragma once

#include <SDL2/SDL_net.h>
#include <SDL2/SDL.h>
#include "players.h"

#define GVX_PORT 8087

enum message_type
  {
    MESSAGE_TYPE_QUERY_ALL,
    MESSAGE_TYPE_UPDATE_TILE,
    MESSAGE_TYPE_UPDATE_PLAYER,
    MESSAGE_TYPE_UPDATE_TILES_METADATA,
    MESSAGE_TYPE_UNAUTHORIZED,
    MESSAGE_TYPE_UPDATE_INDEX,
    MESSAGE_TYPE_MOVE_INTENTION,
    MESSAGE_TYPE_CHANGE_MODE
  };

struct message_tile_data
{
  int x, y, value;
};

struct message_tiles_metadata
{
  int width, height;
};

struct message
{
  enum message_type type;
  union {
    struct message_tile_data tile_data;
    struct message_tiles_metadata tiles_metadata;
    struct player_data player_data;
    enum move_intention move_intention;
    enum player_mode player_mode;
    int index;
  };
};

void gvx_send_query_all (TCPsocket socket);
void gvx_send_update_tile (TCPsocket socket, int x, int y);
void gvx_send_update_tiles_metadata (TCPsocket socket);
void gvx_send_update_player (TCPsocket socket, int i);
void gvx_send_unauthorized (TCPsocket socket);
void gvx_send_update_index (TCPsocket socket, int index);
void gvx_send (TCPsocket socket, struct message message);
void gvx_send_move_intention (TCPsocket, enum move_intention);
void gvx_send_change_mode (TCPsocket, enum player_mode);
