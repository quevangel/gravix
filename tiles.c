#include "tiles.h"

#include <assert.h>

int tiles[TILES_MAX_WIDTH][TILES_MAX_HEIGHT];

int _tiles_width, _tiles_height;

void
init_tiles (int tiles_width, int tiles_height)
{
  assert (_tiles_width < TILES_MAX_WIDTH);
  assert (_tiles_height < TILES_MAX_HEIGHT);
  assert (_tiles_width >= 0);
  assert (_tiles_height >= 0);
  
  _tiles_width = tiles_width;
  _tiles_height = tiles_height;
  for (int x = 0; x < _tiles_width; x++)
    for (int y = 0; y < _tiles_height; y++)
      {
	tiles[x][y] = TILE_UNKNOWN;
      }
}

int
get_tiles_width ()
{
  return _tiles_width;
}

void
set_tiles_width (int width)
{
  _tiles_width = width;
}

int
get_tiles_height ()
{
  return _tiles_height;
}

void
set_tiles_height (int height)
{
  _tiles_height = height;
}

int
get_tile (int x, int y)
{
  if (x >= 0 && x < _tiles_width &&
      y >= 0 && y < _tiles_height)
    return tiles[x][y];
  return TILE_UNKNOWN;
}

int
set_tile (int x, int y, int value)
{
  if (x >= 0 && x < _tiles_width &&
      y >= 0 && y < _tiles_height)
    {
      tiles[x][y] = value;
      return 1;
    }
  return 0;
}
