CC=clang
LIBS=-lm -lSDL2 -lSDL2_net -g -Og
HEADERS=*.h
COMMON_C_FILES=box.c camera.c tiles.c math_util.c players.c net_common.c
SERVER_C_FILES=server.c net_server.c
CLIENT_C_FILES=client.c

all: $(SERVER_C_FILES) $(CLIENT_C_FILES) $(COMMON_C_FILES) $(HEADERS)
	$(CC) $(LIBS) -o server $(SERVER_C_FILES) $(COMMON_C_FILES)
	$(CC) $(LIBS) -o client $(CLIENT_C_FILES) $(COMMON_C_FILES)
