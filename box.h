#pragma once

#include <stdbool.h>

struct box
{
  float center[2];
  float half_size[2];
};
struct box_side
{
  int axis;
  int sign;
};
struct physical_box
{
  struct box box;
  float velocity[2];
};


void
set_side (struct box*, struct box_side, float v);
float
get_side (struct box*, struct box_side);
int
next_integer (float f, int direction);
bool
box_touches_tiles (struct box*);
bool
box_side_touches_tiles (struct box*, struct box_side);
void
physical_box_move (struct physical_box*, float);
void
physical_box_move_axis (struct physical_box*, float, int axis);
void
box_move_until_collision (struct box*, struct box_side);
