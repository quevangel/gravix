#include <SDL2/SDL.h>
#include <SDL2/SDL_net.h>

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <assert.h>
#include <math.h>

#include "net_common.h"
#include "box.h"
#include "tiles.h"
#include "math_util.h"
#include "camera.h"

SDL_Window* window;
SDL_Renderer* renderer;
void
init_sdl (int window_width, int window_height);
void
quit_sdl ();

void
fatal_sdl_error (const char* function_name)
{
  fprintf (stderr, "!! SDL_Error %s (%s)\n",
	   function_name,
	   SDL_GetError ());
  exit(1);
}

int my_player_index = -1;

void
do_tests ()
{
  assert (next_integer (1.5, +1) == 2);
  assert (next_integer (1.5, -1) == 1);
  
  float point[2] = {-6, -6};
  camera_transform_point (point);
  assert ((int) point[0] == 0);
  assert ((int) point[1] == WINDOW_HEIGHT);
  
  point[0] = 0, point[1] = 0;
  camera_transform_point (point);
  assert (fabsf (point[0] - WINDOW_WIDTH/2) < 1.f);
  assert (fabsf (point[1] - WINDOW_HEIGHT/2) < 1.f);
}

TCPsocket socket;


int
message_receiver (void* data)
{
#define DO_CASE(name) case MESSAGE_TYPE_##name: fprintf (stderr, "Received " #name "\n");
  for (;;)
    {
      struct message message;
      int bytes = SDLNet_TCP_Recv (socket, &message, sizeof (message));
      if (bytes <= 0) break;
      switch (message.type)
	{
	  DO_CASE(QUERY_ALL)
	    break;
	  DO_CASE(UPDATE_TILE)
	    {
	      printf ("%d %d = %d\n", message.tile_data.x,
		      message.tile_data.y,
		      message.tile_data.value);
	      int x = message.tile_data.x;
	      int y = message.tile_data.y;
	      int value = message.tile_data.value;
	      set_tile (x, y, value);
	    }
	    break;
	  DO_CASE(UPDATE_PLAYER)
	    {
	      printf ("updating player %d\n",
		      message.player_data.index);
	      player_data[message.player_data.index] = message.player_data;
	    }
	    break;
	  DO_CASE(UPDATE_TILES_METADATA)
	    {
	      printf ("%d %d\n",
		      message.tiles_metadata.width,
		      message.tiles_metadata.height);
	      set_tiles_width (message.tiles_metadata.width);
	      set_tiles_height (message.tiles_metadata.height);
	    }
	    break;
	  DO_CASE(UPDATE_INDEX)
	    {
	      printf ("%d\n",
		      message.index);
	      my_player_index = message.index;
	    }
	    break;
	  DO_CASE(UNAUTHORIZED)
	    break;
	  DO_CASE(MOVE_INTENTION)
	    break;
	  DO_CASE(CHANGE_MODE)
	    break;
	default:
	  fprintf (stderr, "Unknown messsage received\n");
	  break;
	}
    }
  return 0;
}

int
main (int argc, char* argv[])
{
  do_tests ();
  init_sdl (WINDOW_WIDTH, WINDOW_HEIGHT);

  if (SDLNet_Init () == -1)
    {
      printf ("Error initializing SDLNet: %s\n", SDLNet_GetError ());
      return 1;
    }
  if (argc < 2)
    {
      printf ("Ip address required\n");
      return 1;
    }
  char* ip_address_string = argv[1];
  IPaddress ip;
  if (SDLNet_ResolveHost (&ip, ip_address_string, GVX_PORT) == -1)
    {
      printf ("Error resolving host address: %s\n", SDLNet_GetError ());
      return 1;
    }
  socket = SDLNet_TCP_Open (&ip);
  if (socket == NULL)
    {
      printf ("Error connecting to server: %s\n", SDLNet_GetError ());
      return 1;
    }
  SDL_Thread *message_thread = SDL_CreateThread (message_receiver,
						 "message_receiver",
						 NULL);
#define FPS 100
#define MS_PER_FRAME (1000 / FPS)
#define SECS_PER_FRAME (1.f / FPS)
  const Uint8 *keyboard_state = SDL_GetKeyboardState (NULL);
  int last_ticks = SDL_GetTicks ();
  int mouse_integer_position[2];
  float mouse_position[2];
  Uint32 mouse_buttons_state = 0;
#define MOUSE_PRESSED(x) (mouse_buttons_state & SDL_BUTTON(SDL_BUTTON_##x))
#define MOUSE_INSIDE_TILES (mouse_integer_position[0] >= 0 && mouse_integer_position[1] >= 0 && mouse_integer_position[0] < get_tiles_width () && mouse_integer_position[1] < get_tiles_height ())
#define MOUSE_TILE tiles[mouse_integer_position[0]][mouse_integer_position[1]]
  for (;;)
    {
      // events::
      SDL_Event event;
      int jumped = 0;
      while (SDL_PollEvent (&event))
	{
	  switch (event.type) 
	    {
	    case SDL_QUIT:
	      goto end_game;
	      break;
	    case SDL_KEYDOWN:
	      if (event.key.keysym.scancode == SDL_SCANCODE_N)
		{
		  gvx_send_change_mode (socket, PLAYER_MODE_NORMAL);
		}
	      break;
	    default:
	      break;
	    }
	}
      
      mouse_buttons_state = SDL_GetMouseState (&mouse_integer_position[0],
			 &mouse_integer_position[1]);
      mouse_position[0] = mouse_integer_position[0];
      mouse_position[1] = mouse_integer_position[1];
      camera_inverse_transform_point (mouse_position);
      mouse_integer_position[0] = floor (mouse_position[0]);
      mouse_integer_position[1] = floor (mouse_position[1]);

      if (MOUSE_PRESSED(LEFT))
	{
	  int x = mouse_integer_position[0];
	  int y = mouse_integer_position[1];
	  if (set_tile (x, y, 1))
	    gvx_send_update_tile (socket, x, y);
	}
      
      if (MOUSE_PRESSED(RIGHT))
	{
	  int x = mouse_integer_position[0];
	  int y = mouse_integer_position[1];
	  if (set_tile (x, y, 0))
	    gvx_send_update_tile (socket, x, y);
	}

#define INPUT_MOVE(x)							\
      if (keyboard_state[SDL_SCANCODE_##x])				\
	{								\
	  fprintf (stderr, "Sending move intention " #x);		\
	  gvx_send_move_intention (socket, MOVE_INTENTION_##x);		\
	}
      
      INPUT_MOVE(LEFT);
      INPUT_MOVE(DOWN);
      INPUT_MOVE(RIGHT);
      INPUT_MOVE(UP);

#define CAMERA_ZOOM_FACTOR 1.01
      if (keyboard_state[SDL_SCANCODE_EQUALS])
	{
	  camera.half_size[0] /= CAMERA_ZOOM_FACTOR;
	  camera.half_size[1] /= CAMERA_ZOOM_FACTOR;
	}
      if (keyboard_state[SDL_SCANCODE_MINUS])
	{
	  camera.half_size[0] *= CAMERA_ZOOM_FACTOR;
	  camera.half_size[1] *= CAMERA_ZOOM_FACTOR;
	}
#undef CAMERA_ZOOM_FACTOR
      // follow player with camera
      if (my_player_index != -1)
      {
	struct box player = player_data[my_player_index].physical_box.box;
	#define FOLLOW_FACTOR 0.01
	camera.center[0] += FOLLOW_FACTOR * (player.center[0] - camera.center[0]);
	camera.center[1] += FOLLOW_FACTOR * (player.center[1] - camera.center[1]);
	#undef FOLLOW_FACTOR
      }
      // RENDERING
      // clear window
      SDL_SetRenderDrawColor (renderer, 0, 0, 0, 255);
      SDL_RenderClear (renderer);
      // render player 
      SDL_Rect rect;
      
      for (int player_index = 0;
	   player_index < MAX_NO_PLAYERS;
	   player_index++)
	if (player_data[player_index].alive)
	  {
	    struct box player = player_data[player_index].physical_box.box;
	    camera_box_to_sdl_rect (&player, &rect);
	    SDL_SetRenderDrawColor (renderer, 255, 255, 255, 255);
	    SDL_RenderFillRect (renderer, &rect);
	  }
      // render tiles
      float camera_low[2], camera_high[2];
      int camera_low_i[2], camera_high_i[2];
      camera_low[0] = camera.center[0] - camera.half_size[0];
      camera_low[1] = camera.center[1] - camera.half_size[1];
      camera_low_i[0] = floor (camera_low[0]);
      camera_low_i[1] = floor (camera_low[1]);
      
      camera_high[0] = camera.center[0] + camera.half_size[0];
      camera_high[1] = camera.center[1] + camera.half_size[1];
      camera_high_i[0] = floor (camera_high[0]);
      camera_high_i[1] = floor (camera_high[1]);
      
      for (int x = camera_low_i[0]; x <= camera_high_i[0]; x++)
	for (int y = camera_low_i[1]; y <= camera_high_i[1]; y++)
	  {
	    if (get_tile (x, y))
	      {
		if ((x^y) & 1)
		  SDL_SetRenderDrawColor (renderer, 255, 255, 0, 255);
		else
		  SDL_SetRenderDrawColor (renderer, 128, 128, 0, 255);
		
		if (get_tile (x, y) == TILE_UNKNOWN)
		  {
		    SDL_SetRenderDrawColor (renderer, 128, 128, 128, 255);
		  }
		
		struct box tile_box = {{x + 0.5f, y + 0.5f}, {0.5f, 0.5f}};
		camera_box_to_sdl_rect (&tile_box, &rect);
		SDL_RenderFillRect (renderer, &rect);
	      }
	  }
      // end render
      // render selection
      {
	int x = mouse_integer_position[0];
	int y = mouse_integer_position[1];
	SDL_SetRenderDrawColor (renderer, 255, 0, 0, 128);
	struct box tile_box = {{x + 0.5f, y + 0.5f}, {0.5f, 0.5f}};
	camera_box_to_sdl_rect (&tile_box, &rect);
	SDL_RenderFillRect (renderer, &rect);
      }
      
      SDL_RenderPresent (renderer);
      // Delay
      int current_ticks = SDL_GetTicks ();
      int delay = MS_PER_FRAME - (current_ticks - last_ticks);
      if (delay >= 0)
	SDL_Delay (delay);
      else
	fprintf (stderr, "Underperformance by %d ms\n", -delay);
      last_ticks = SDL_GetTicks ();
    }
 end_game:
  quit_sdl ();
  return 0;
}

void
init_sdl (int window_width, int window_height)
{
  if (SDL_Init (SDL_INIT_EVERYTHING) < 0)
    fatal_sdl_error ("SDL_Init()");
  if (SDL_CreateWindowAndRenderer (window_width, window_height, 0, &window, &renderer) < 0)
    fatal_sdl_error ("SDL_CreateWindowAndRenderer()");
  SDL_SetRenderDrawBlendMode (renderer, SDL_BLENDMODE_ADD);
}

void
quit_sdl ()
{
  SDL_Quit ();
}

