#pragma once

#include "box.h"

#define MAX_NO_PLAYERS 100

enum player_mode
  {
    PLAYER_MODE_FLOATING,
    PLAYER_MODE_NORMAL
  };
enum move_intention
  {
    MOVE_INTENTION_RIGHT,
    MOVE_INTENTION_UP,
    MOVE_INTENTION_LEFT,
    MOVE_INTENTION_DOWN
  };

struct player_data
{
  bool alive;
  enum player_mode mode;
  struct physical_box physical_box;
  int move_intention_bits;
  int index;
};

extern struct player_data player_data[MAX_NO_PLAYERS];
void init_player_data ();
int register_new_player ();
