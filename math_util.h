#pragma once

#define SIGN(x) ((x)>=0?1:-1)

struct affine_transform
{
  float multiplier;
  float adder;
};

struct affine_transform
linear_interpolation (float a, float b, float a_res, float b_res);

float
apply_affine_transform (float v, struct affine_transform transform);

int
next_integer (float f, int direction);

int
get_cell (float x, int side);
