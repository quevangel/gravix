#include <stdio.h>
#include <stdlib.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_net.h>

#include "net_common.h"
#include "net_server.h"
#include "players.h"
#include "tiles.h"
#include "box.h"

#define IS_NULL(x) ((x) == NULL)
#define IS_MONE(x) ((x) == -1)
#define IS_NEGATIVE(x) ((x) < 0)

#define SDLNET(function, error_condition, ...) ({			\
      __auto_type return_value = SDLNet_ ## function (__VA_ARGS__);	\
 if (error_condition(return_value))					\
  {									\
    fprintf (stderr, "SDLNet_" #function "! %s\n", SDLNet_GetError ());	\
    exit (1);								\
  }									\
 return_value;								\
})

#define SDL(function, error_condition, ...)  ({			\
      __auto_type return_value = SDL_ ## function (__VA_ARGS__);	\
 if (error_condition(return_value))					\
  {									\
    fprintf (stderr, "SDL_" #function "! %s\n", SDL_GetError ());	\
    exit (1);								\
  }									\
 return_value;								\
})

int
lobby (void *data);

int
message_getter (void *client_index);

void
do_simulation ();

int
main (int argc, char* argv[])
{
  init_tiles (100, 100);
  for (int i = 0; i < 100; i++)
    for (int j = 0; j < 100; j++)
      tiles[i][j] = 1;

  SDL(Init, IS_NEGATIVE, SDL_INIT_EVERYTHING);
  SDLNET(Init, IS_MONE);
  IPaddress ip;
  SDLNET(ResolveHost, IS_MONE, &ip, NULL, GVX_PORT);
  server_socket = SDLNET(TCP_Open, IS_NULL, &ip);
  SDL_Thread* lobby_thread = SDL(CreateThread, IS_NULL, lobby, "lobby thread", NULL);
  SDL_DetachThread (lobby_thread);
  while (true)
    {
      do_simulation ();
    }
  return 0;
}

int
lobby (void* _data)
{
  for (;;)
    {
      TCPsocket new_client = NULL;
      fprintf (stderr, "Waiting for a new client...\n");
      while (new_client == NULL)
	{
	  SDL_Delay (100);
	  new_client = SDLNet_TCP_Accept (server_socket);
	}
      fprintf (stderr, "New client received...\n");
      socket_to_client[no_clients++] = new_client;
      long client_index = no_clients - 1;
      SDL_Thread *new_client_thread = SDL(CreateThread,
					  IS_NULL,
					  message_getter,
					  "message receiver",
					  (void*)client_index);
      SDL_DetachThread (new_client_thread);
    }
}

int
message_getter (void *client_index_voidptr)
{
  long client_index = (long) client_index_voidptr;
  fprintf (stderr, "Started message_getter for client %d\n",
	   (int)client_index);
  register_new_player ();
  gvx_server_send_all (client_index);
  for (int i = 0; i < no_clients; i++)
    {
      for (int j = 0; j < MAX_NO_PLAYERS; j++)
	{
	  if (player_data[j].alive)
	    gvx_send_update_player (socket_to_client[i], j);
	}
    }
  struct message message;
  for (;;)
    {
      int bytes = SDLNet_TCP_Recv (socket_to_client[client_index],
				   &message,
				   sizeof (struct message));
      handle_message (client_index, &message);
      if (bytes <= 0)
	{
	  SDLNet_TCP_Close (socket_to_client[client_index]);
	  return 1;
	}
    }
  return 0;
}


#define FPS 60
#define SECONDS_PER_FRAME (1.f / FPS)
#define MS_PER_FRAME (1000 / FPS)

void
simulate_player (int i);

void
do_simulation ()
{
  int frame_start_ticks = SDL_GetTicks ();
  for (int i = 0; i < no_clients; i++)
    {
      simulate_player (i);
    }
  int frame_end_ticks = SDL_GetTicks ();
  int delta = frame_end_ticks - frame_start_ticks;
  int ticks_remaining_in_frame = MS_PER_FRAME - delta;
  if (ticks_remaining_in_frame > 0)
    SDL_Delay (ticks_remaining_in_frame);
}
#define PLAYER_VELOCITY 5
#define PLAYER_ACCELERATION 2
#define PLAYER_JUMP_VELOCITY 8
void
simulate_player (int i)
{
#define wants_to_move(direction) (player->move_intention_bits & (1 << MOVE_INTENTION_##direction))
  struct player_data* player = &player_data[i];
  if (!(player->alive)) return;
  switch (player->mode)
    {
    case PLAYER_MODE_FLOATING:
      if (wants_to_move (LEFT))
	player->physical_box.box.center[0] -= PLAYER_VELOCITY * SECONDS_PER_FRAME;
      if (wants_to_move (DOWN))
	player->physical_box.box.center[1] -= PLAYER_VELOCITY * SECONDS_PER_FRAME;
      if (wants_to_move (RIGHT))
	player->physical_box.box.center[0] += PLAYER_VELOCITY * SECONDS_PER_FRAME;
      if (wants_to_move (UP))
	player->physical_box.box.center[1] += PLAYER_VELOCITY * SECONDS_PER_FRAME;
      break;
    case PLAYER_MODE_NORMAL:
      if (wants_to_move (LEFT))
	player->physical_box.velocity[0] -= PLAYER_ACCELERATION * SECONDS_PER_FRAME;
      if (wants_to_move (RIGHT))
	player->physical_box.velocity[0] += PLAYER_ACCELERATION * SECONDS_PER_FRAME;
      if (wants_to_move (UP))
	{
	  if (box_side_touches_tiles (&player->physical_box.box,
				      (struct box_side) {1, -1}))
	    {
	      player->physical_box.velocity[1] += PLAYER_JUMP_VELOCITY;
	    }
	}
      #define GRAVITY_Y -9
      player->physical_box.velocity[1] += GRAVITY_Y * SECONDS_PER_FRAME;
      physical_box_move (&player->physical_box, SECONDS_PER_FRAME);
      break;
    }
  player->move_intention_bits = 0;

  for (int j = 0; j < no_clients; j++)
    gvx_send_update_player (socket_to_client[j], i);
}
