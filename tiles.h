#pragma once

#define TILES_MAX_WIDTH 100
#define TILES_MAX_HEIGHT 100
#define TILE_UNKNOWN -1

extern int tiles[TILES_MAX_WIDTH][TILES_MAX_HEIGHT];

void
init_tiles (int tiles_width, int tiles_height);

int
get_tiles_width ();

void
set_tiles_width (int width);

int
get_tiles_height ();

void
set_tiles_height (int height);

int
get_tile (int x, int y);

int
set_tile (int x, int y, int value);
