#include "players.h"

struct player_data player_data[MAX_NO_PLAYERS];

struct player_data default_player_data = (struct player_data) {
  .alive = false,
  .mode = PLAYER_MODE_FLOATING,
  .physical_box = {
    .box = {
      .center = {0, 0},
      .half_size = {0.5, 1.5}
    },
    .velocity = {0, 0}
  },
  .move_intention_bits = 0,
  .index = -1
};

void
init_players_data ()
{
  for (int i = 0; i < MAX_NO_PLAYERS; i++)
    {
      player_data[i] = default_player_data;
      player_data[i].index = i;
    }
}

int
register_new_player ()
{
  for (int i = 0; i < MAX_NO_PLAYERS; i++)
    {
      if (!player_data[i].alive)
	{
	  player_data[i] = default_player_data;
	  player_data[i].alive = true;
	  player_data[i].index = i;
	  return i;
	}
    }
  return -1;
}
