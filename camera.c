#include "camera.h"

#include <assert.h>

#include <SDL2/SDL.h>

#include "math_util.h"

struct box camera = {
  {0, 0},
  {6, 6}
};

void
camera_transform_point (float point[2])
{
  struct affine_transform transform[2];
  transform[0] = linear_interpolation (camera.center[0] - camera.half_size[0],
				       camera.center[0] + camera.half_size[0],
				       0,
				       WINDOW_WIDTH);
  transform[1] = linear_interpolation (camera.center[1] - camera.half_size[1],
				       camera.center[1] + camera.half_size[1],
				       WINDOW_HEIGHT,
				       0);
  point[0] = apply_affine_transform (point[0], transform[0]);
  point[1] = apply_affine_transform (point[1], transform[1]);
}

void
camera_inverse_transform_point (float point[2])
{
  struct affine_transform transform[2];
  
  transform[0] = linear_interpolation (0,
				       WINDOW_WIDTH,
				       camera.center[0] - camera.half_size[0],
				       camera.center[0] + camera.half_size[0]);
  
  transform[1] = linear_interpolation (WINDOW_HEIGHT,
				       0,
				       camera.center[1] - camera.half_size[1],
				       camera.center[1] + camera.half_size[1]);
  
  point[0] = apply_affine_transform (point[0], transform[0]);
  point[1] = apply_affine_transform (point[1], transform[1]);
}

void
camera_box_to_sdl_rect (struct box* box, SDL_Rect* rect)
{
  float lower_pixel[2], higher_pixel[2], length[2];
  
  lower_pixel[0] =  box->center[0] - box->half_size[0];
  lower_pixel[1] =  box->center[1] + box->half_size[1];
  higher_pixel[0] = box->center[0] + box->half_size[0];
  higher_pixel[1] = box->center[1] - box->half_size[1];
  
  camera_transform_point (lower_pixel);
  camera_transform_point (higher_pixel);
  
  length[0] = (int)higher_pixel[0] - (int)lower_pixel[0];
  length[1] = (int)higher_pixel[1] - (int)lower_pixel[1];

  assert (length[0] >= 0);
  assert (length[1] >= 0);
  rect->x = lower_pixel[0];
  rect->y = lower_pixel[1];
  rect->w = length[0];
  rect->h = length[1];
}
