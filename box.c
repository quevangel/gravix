#include "box.h"

#include <math.h>

#include "tiles.h"
#include "math_util.h"

void
set_side (struct box* box, struct box_side box_side, float new_value)
{
  box->center[box_side.axis] = new_value - box_side.sign * box->half_size[box_side.axis];
}

float
get_side (struct box* box, struct box_side box_side)
{
  return box->center[box_side.axis] + box_side.sign * box->half_size[box_side.axis];
}

bool
box_touches_tiles (struct box* box)
{
  int lower_x = (int) ceil (box->center[0] - box->half_size[0]) - 1;
  if (lower_x < 0) lower_x = 0;
  int higher_x = (int) floor (box->center[0] + box->half_size[0]);
  if (higher_x >= get_tiles_width ()) higher_x = get_tiles_width () - 1;
  
  int lower_y = (int) ceil (box->center[1] - box->half_size[1]) - 1;
  if (lower_y < 0) lower_y = 0;
  int higher_y = (int) floor (box->center[1] + box->half_size[1]);
  if (higher_y >= get_tiles_height ()) higher_y = get_tiles_height () - 1;

  for (int x = lower_x; x <= higher_x; x++)
    for (int y = lower_y; y <= higher_y; y++)
      {
	if (tiles[x][y]) return true;
      }
  return false;
}

bool
box_intersects_tiles (struct box* box)
{
  int lower_x = (int) floor (box->center[0] - box->half_size[0]);
  if (lower_x < 0) lower_x = 0;
  int higher_x = (int) ceil (box->center[0] + box->half_size[0]) - 1;
  if (higher_x >= get_tiles_width ()) higher_x = get_tiles_width () - 1;
  
  int lower_y = (int) floor (box->center[1] - box->half_size[1]);
  if (lower_y < 0) lower_y = 0;
  int higher_y = (int) ceil (box->center[1] + box->half_size[1]) - 1;
  if (higher_y >= get_tiles_height ()) higher_y = get_tiles_height () - 1;

  for (int x = lower_x; x <= higher_x; x++)
    for (int y = lower_y; y <= higher_y; y++)
      {
	if (tiles[x][y]) return true;
      }
  return false;
}

void
physical_box_move (struct physical_box* physical_box, float time_delta)
{
  physical_box_move_axis (physical_box, time_delta, 0);
  physical_box_move_axis (physical_box, time_delta, 1);
}

void
physical_box_move_axis (struct physical_box* physical_box, float time_delta, int axis)
{
  if (box_intersects_tiles (&physical_box->box))
    {
      physical_box->velocity[0] = 0;
      physical_box->velocity[1] = 0;
      return;
    }
  struct box hypothetical_box = physical_box->box;
  hypothetical_box.center[axis] += physical_box->velocity[axis] * time_delta;
  if (!box_intersects_tiles (&hypothetical_box))
    {
      physical_box->box = hypothetical_box;
      return;
    }
  int sign = SIGN(physical_box->velocity[axis]);
  physical_box->velocity[axis] = 0;
  box_move_until_collision (&physical_box->box,
			    (struct box_side)
			    {.axis = axis,
			     .sign = sign});
}

void
box_move_until_collision (struct box* box, struct box_side box_side)
{
  set_side (box, box_side, next_integer (get_side (box, box_side), box_side.sign));
  while (!box_side_touches_tiles (box, box_side))
    {
      set_side (box, box_side, get_side (box, box_side) + box_side.sign);
    }
  return;
}

bool
box_side_touches_tiles (struct box* box, struct box_side box_side)
{
  int other_axis = box_side.axis ^ 1;
  int side_position = get_cell (get_side (box, box_side), box_side.sign);
  int lower = (int) floor (get_side (box, (struct box_side) {other_axis, -1}));
  int higher = (int) ceil (get_side (box, (struct box_side) {other_axis, +1})) - 1;
  int index[2];
  index[box_side.axis] = side_position;
  for (int i = lower; i <= higher; i++)
    {
      index[other_axis] = i;
      if (index[0] >= 0 && index[0] < get_tiles_width () &&
	  index[1] >= 0 && index[1] < get_tiles_height ())
	if (tiles[index[0]][index[1]]) return true;
    }
  return false;
}
